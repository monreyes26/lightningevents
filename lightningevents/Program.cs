﻿
using lightningevents.Model;
using lightningevents.Services;
using Newtonsoft;

// See https://aka.ms/new-console-template for more information

bool continueProcess = true;

while (continueProcess)
{
    Console.Write("Please enter folder path contains  lightning.data and Assets in json format or press enter to use the default file\r\nFolder Path :");

    var dataFolderPath = Console.ReadLine();

    if (string.IsNullOrEmpty(dataFolderPath))
    {
        dataFolderPath = Path.Combine(System.Environment.CurrentDirectory, "Data");
        Console.WriteLine(dataFolderPath);
    }


    ProcessAlert processAlert = new ProcessAlert();
    List<Asset> assets = processAlert.GetAssetData(dataFolderPath);

    string lightningDataPath = Path.Combine(dataFolderPath, "lightning.json");
    using (StreamReader reader = new StreamReader(lightningDataPath))
    {
       List<Alert> alerts = processAlert.StartProcess(assets, reader);     
        
        foreach(Alert alert in alerts)
        {
            Console.WriteLine($"lightning alert for {alert.Owner}:{alert.Name}");
        }
    }



    Console.WriteLine("Do you want to do another the process?\r\n0=No\r\n1=Yes");

    var reportProcess = Console.ReadLine();

    if (Convert.ToInt32(reportProcess) == 0)
        continueProcess = false;
}

