﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lightningevents.Model
{
    public class Alert
    {
       public string? Name { get; set; }        

       public  string? Owner { get; set; }
    }
}
