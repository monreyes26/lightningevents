﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lightningevents.Model
{
    public class Strike
    {
        public int FlashType { get; set; }
        public double strikeTime { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int peakAmps { get; set; }
        public string? reserved { get; set; }
        public int icHeight { get; set; }
        public double receivedTime { get; set; }
        public int numberOfSensors { get; set; }
        public int multiplicity { get; set; }
    }
}
