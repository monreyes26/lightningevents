﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lightningevents.Model
{
    public class Asset
    {
        public string assetName { get; set; }

        public string quadKey { get; set; }

        public string assetOwner { get; set; }
    }
}