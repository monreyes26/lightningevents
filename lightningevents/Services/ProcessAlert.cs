﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lightningevents.Model;
using Microsoft.MapPoint;
using Newtonsoft.Json;


namespace lightningevents.Services
{
    public class ProcessAlert
    {        
        public ProcessAlert()
        {
           
        }
        
        public List<Alert> StartProcess(List<Asset> assets, StreamReader reader)
        {
            List<Alert> _alerts = new List<Alert>();
            string? line = string.Empty;
        
            while ((line = reader.ReadLine()) != null)
            {
                Strike strike = JsonConvert.DeserializeObject<Strike>(line);
                string quadkey = GetQuadKey(strike.latitude, strike.longitude, 12);
                Asset asset = assets.Where(a => a.quadKey.Equals(quadkey)).FirstOrDefault();

                if ( asset != null)
                {
                    Alert alert = new Alert();
                    alert.Name = asset.assetName;
                    alert.Owner = asset.assetOwner;

                    if(!(_alerts.Any(a => a.Name.Equals(alert.Name) && a.Owner.Equals(alert.Owner))))
                        _alerts.Add(alert);                      
                }
            }

            return _alerts;
        }
        public List<Asset> GetAssetData(string dataFolderPath)
        {
            string assetDataPath = Path.Combine(dataFolderPath, "assets.json");

            using (StreamReader r = new StreamReader(assetDataPath))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<Asset>>(json);
            }
        } 

        private string GetQuadKey(double latitude, double longitude, int level)
        {           
            TileSystem.LatLongToPixelXY(latitude, longitude, level, out int pixelX, out int pixelY);
            TileSystem.PixelXYToTileXY(pixelX, pixelY, out int tileX, out int tileY);
            return TileSystem.TileXYToQuadKey(tileX, tileY, level);            
        }

    }
}
