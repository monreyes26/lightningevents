using lightningevents.Model;
using NUnit.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using lightningevents.Services;
using Moq;
using System.IO;
using System.Text;
using System.Linq;

namespace LigthningEventUnitTest
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void AssetFoundWriteAlert()
        {
            var assetsJson = "[{'assetName':'Mayer Park','quadKey':'023112133002','assetOwner':'02115'},{'assetName':'Sunshine Wall','quadKey':'023112133003','assetOwner':'325'}]";
            var strikeJson = "{'flashType':1,'strikeTime':1446761094139,'latitude':34.7599419,'longitude':-96.2856216,'peakAmps':1886,'reserved':'000','icHeight':12005,'receivedTime':1446761106118,'numberOfSensors':20,'multiplicity':32}\r\n" +
                             "{'flashType':1,'strikeTime':1446761094205,'latitude':34.7963334,'longitude':-96.2253683,'peakAmps':-2796,'reserved':'000','icHeight':16810,'receivedTime':1446761106118,'numberOfSensors':16,'multiplicity':32}";

            List<Asset> assets = JsonConvert.DeserializeObject<List<Asset>>(assetsJson);

            byte[] byteArray = Encoding.UTF8.GetBytes(strikeJson);
            MemoryStream stream = new MemoryStream(byteArray);
            StreamReader reader = new StreamReader(stream);
            
            ProcessAlert processAlert = new ProcessAlert();
            var result = processAlert.StartProcess(assets, reader);

            Assert.IsTrue(result.Count > 0);
        }

        [Test]
        public void RepeatedAlertShouldnotIncluded()
        {
            var assetsJson = "[{'assetName':'Mayer Park','quadKey':'023112133002','assetOwner':'02115'},{'assetName':'Sunshine Wall','quadKey':'023112133003','assetOwner':'325'}]";
            var strikeJson = "{'flashType':1,'strikeTime':1446761094139,'latitude':34.7599419,'longitude':-96.2856216,'peakAmps':1886,'reserved':'000','icHeight':12005,'receivedTime':1446761106118,'numberOfSensors':20,'multiplicity':32}\r\n" +
                             "{'flashType':1,'strikeTime':1446761094205,'latitude':34.7963334,'longitude':-96.2253683,'peakAmps':-2796,'reserved':'000','icHeight':16810,'receivedTime':1446761106118,'numberOfSensors':16,'multiplicity':32}\r\n" +
                             "{'flashType':1,'strikeTime':1446761094139,'latitude':34.7599419,'longitude':-96.2856216,'peakAmps':1886,'reserved':'000','icHeight':12005,'receivedTime':1446761106118,'numberOfSensors':20,'multiplicity':32}\r\n" +
                             "{'flashType':1,'strikeTime':1446761094205,'latitude':34.7963334,'longitude':-96.2253683,'peakAmps':-2796,'reserved':'000','icHeight':16810,'receivedTime':1446761106118,'numberOfSensors':16,'multiplicity':32}";

            List<Asset> assets = JsonConvert.DeserializeObject<List<Asset>>(assetsJson);

            byte[] byteArray = Encoding.UTF8.GetBytes(strikeJson);
            MemoryStream stream = new MemoryStream(byteArray);
            StreamReader reader = new StreamReader(stream);

            ProcessAlert processAlert = new ProcessAlert();
            var result = processAlert.StartProcess(assets, reader);

            Assert.IsTrue(result.Count == 2);
        }
    }
}